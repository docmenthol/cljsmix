(ns cljsmix.db
  (:require [cljs.spec.alpha :as s]
            [re-frame.core :as rf]))

(s/def ::id int?)
(s/def ::name string?)
(s/def ::percent float?)
(s/def ::flavor
  (s/keys :req-un [::name ::percent]))
(s/def ::flavors
  (s/and
   (s/map-of ::id ::flavor)
   #(map? %)))

(s/def ::nic float?)
(s/def ::target float?)
(s/def ::amount float?)
(s/def ::pg float?)
(s/def ::vg float?)
(s/def ::parameters
  (s/keys :req-un [::nic ::target ::amount ::pg ::vg]))

(s/def ::recipe
  (s/keys :req-un [::name ::flavors]))
(s/def ::recipes
  (s/and
   (s/map-of ::id ::recipe)
   #(map? %)))

(s/def ::db (s/keys :req-un [::flavors ::recipes ::parameters]))

(def default-db
  {:recipes    {}
   :flavors    {}
   :parameters {:nic    100.0
                :target 3.0
                :amount 60.0
                :pg     0.0
                :vg     100.0}})

(def empty-flavor
  {:name    ""
   :percent 0.0})
