(ns cljsmix.views-common
  (:require [cljsmix.views-desktop :as desktop]
            [cljsmix.views-mobile :as mobile]
            [cljsmix.lib.semantic :as sem]
            [cljsmix.lib.semantic-helpers :as sh]))

;; Sidebar Notebook
(defn notebook []
  [:div
   (sh/menu-icon {:as "div"} {:name "tint"} "Recipes")
   (sh/menu-icon {:as "a"}   {:name "add circle"})
   (sh/menu-icon {:as "a"}   {:name "download"})
   (sh/menu-icon {:as "a"}   {:name "upload"})])

;; Main Mixer Component
(defn mixer []
  [:> sem/grid {:divided "vertically"}
   [:> sem/grid-row
    [:> sem/grid-column
     [:> sem/responsive {:max-width 991} (mobile/mixer)]
     [:> sem/responsive {:min-width 992} (desktop/mixer)]]]
   (mobile/mixer-manager) (desktop/mixer-manager)])

;; Recipe Output Table
(defn output []
  (sh/table
   {:celled true
    :inverted true
    :selectable true
    :striped true
    :unstackable true}
   [[{:text-align "center"} "Ingredient"]
    [{:text-align "center" :width 2} "mL"]
    [{:text-align "center" :width 2} "g"]]
   [[{:text-align "right"} [:strong "Total"]]
    [{:text-align "right"  :width 2} "--"]
    [{:text-align "right"  :width 2} "--"]]))
