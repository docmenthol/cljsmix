(ns cljsmix.views
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [cljsmix.lib.semantic :as sem]
            [cljsmix.lib.semantic-helpers :as sh]
            [cljsmix.views-common :as common]))

;; Mobile Page Layout
(defn mobile-layout []
  [:> sem/grid {:columns 1}
   [:> sem/grid-row
    (sh/animated-button {:basic true
                         :style {:margin-bottom "1em"
                                 :margin-left   "1em"}}
                        "chevron right"
                        "sidebar")]
   [:> sem/grid-row
    [:> sem/grid-column (common/mixer)]]
   [:> sem/grid-row
    [:> sem/grid-column (common/output)]]])

;; Desktop Page Layout
(defn desktop-layout []
  [:> sem/grid {:columns 2}
   [:> sem/grid-column {:width 10} (common/mixer)]
   [:> sem/grid-column {:width 6}  (common/output)]])

;; Main App Layout
(defn layout []
  [:> sem/container {:style {:margin-top "2em"}}
   [:> sem/responsive {:max-width 1080} (mobile-layout)]
   [:> sem/responsive {:min-width 1081} (desktop-layout)]])

;; Main App
(defn app []
  [:> sem/responsive {:fire-on-mount true}
   [:> sem/sidebar-pushable {:style {:min-height "100vh"}}
    [:> sem/sidebar
     {:as sem/menu
      :animation "overlay"
      :icon "labeled"
      :width "thin"
      :inverted true
      :vertical true
      :visible true}
     (common/notebook)]
    [:> sem/sidebar-pusher
     [:> sem/ref (layout)]]]])
