(ns cljsmix.lib.semantic-helpers
  (:require [cljsmix.lib.semantic :as sem]))

(defn menu-icon [item-props icon-props & children]
  [:> sem/menu-item item-props
   [:> sem/icon icon-props] children])

(defn grid-cols
  ([row-component row-props]
   (for [rp row-props]
     [:> sem/grid-column
      [:> row-component rp]]))
  ([row-component col-props row-props]
   (let [pairs (map vector col-props row-props)]
     (for [p pairs]
       [:> sem/grid-column (first p) [:> row-component (second p)]]))))

(defn table-rows [component cells]
  [:> sem/table-row
   (for [c cells]
     [:> component (first c) (second c)])])

(defn table
  ([props body-cells]
   [:> sem/table props
    (table-rows sem/table-cell body-cells)])
  ([props header-cells body-cells]
   [:> sem/table props
    [:thead
     (table-rows sem/table-header-cell header-cells)]
    [:tbody
     (table-rows sem/table-cell body-cells)]]))

(defn animated-button [props shown-icon hidden-icon]
  [:> sem/button (merge props {:animated true})
   [:> sem/button-content {:hidden true}
    [:> sem/icon {:name hidden-icon}]]
   [:> sem/button-content {:visible true}
    [:> sem/icon {:name shown-icon}]]])
