(ns cljsmix.lib.semantic
  (:require [cljsjs.semantic-ui-react]
            [goog.object]))

(def semantic-ui js/semanticUIReact)

(defn semantic-component [k & ks]
  (if (seq ks)
    (apply goog.object/getValueByKeys semantic-ui k ks)
    (goog.object/get semantic-ui k)))

(def sidebar           (semantic-component "Sidebar"))
(def sidebar-pushable  (semantic-component "Sidebar" "Pushable"))
(def sidebar-pusher    (semantic-component "Sidebar" "Pusher"))
(def menu              (semantic-component "Menu"))
(def menu-item         (semantic-component "Menu" "Item"))
(def container         (semantic-component "Container"))
(def grid              (semantic-component "Grid"))
(def grid-row          (semantic-component "Grid" "Row"))
(def grid-column       (semantic-component "Grid" "Column"))
(def button            (semantic-component "Button"))
(def button-content    (semantic-component "Button" "Content"))
(def ref               (semantic-component "Ref"))
(def icon              (semantic-component "Icon"))
(def input             (semantic-component "Input"))
(def responsive        (semantic-component "Responsive"))
(def table             (semantic-component "Table"))
(def table-header      (semantic-component "Table" "Header"))
(def table-row         (semantic-component "Table" "Row"))
(def table-header-cell (semantic-component "Table" "HeaderCell"))
(def table-body        (semantic-component "Table" "Body"))
(def table-cell        (semantic-component "Table" "Cell"))
