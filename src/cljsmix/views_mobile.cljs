(ns cljsmix.views-mobile
  (:require [re-frame.core :as rf]
            [cljsmix.lib.semantic :as sem]
            [cljsmix.lib.semantic-helpers :as sh]))

(defn flavor-input [flavor]
  "Mobile flavor input form."
  [:> sem/grid-row
   [:> sem/grid-column {:width 11}
    [:> sem/input {:placeholder "Unnamed Flavor"
                   :value (:name flavor)
                   :fluid true
                   :on-change #(rf/dispatch [:update-flavor flavor :name (.. % -target -value)])}]]
   [:> sem/grid-column {:width 3
                        :style {:padding-left 5
                                :padding-right 20}}
    [:> sem/input {:placeholder "%"
                   :value (:percent flavor)
                   :fluid true
                   :on-change #(rf/dispatch [:update-flavor flavor :percent (js/Number (.. % -target -value))])}]]
   [:> sem/grid-column {:width 2
                         :vertical-align "middle"
                         :style {:padding-left 0}}
    [:> sem/button {:size "tiny"
                    :icon "remove"
                    :on-click #(rf/dispatch [:remove-flavor flavor])}]]])

(defn mixer []
  "Mobile parameter and flavor inputs."
  (let [parameters @(rf/subscribe [:parameters])]
    [:> sem/grid {:columns 3}
     [:> sem/grid-row
      (sh/grid-cols
       sem/input
       [{:label "nic" :fluid true :value (:nic parameters)}
        {:label "tgt" :fluid true :value (:target parameters)}
        {:label "tgt" :fluid true :value (:target parameters)}])]]
    [:> sem/grid {:columns 2}
     [:> sem/grid-row
      (sh/grid-cols
       sem/input
       [{:label "pg" :fluid true :value (:pg parameters)}
        {:label "vg" :fluid true :value (:vg parameters)}])]]))

(defn mixer-manager []
  "Mobile Add Flavor and Clear Flavors buttons."
  [:> sem/responsive
   {:as sem/grid-row :max-width 991}
   (sh/grid-cols
    sem/button
    [{:width 14}
     {:width 2 :style {:padding-left "0px"}}]
    [{:size "tiny" :icon "add"             :on-click #(rf/dispatch [:add-flavor])}
     {:size "tiny" :icon "trash alternate" :on-click #(rf/dispatch [:clear-flavors])}])])
