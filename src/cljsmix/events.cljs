(ns cljsmix.events
  (:require [re-frame.core :as rf]
            [cljs.spec.alpha :as s]
            [cljsmix.db :as db]))

(defn check-and-throw
  "Validates the app state given a spec and throws if invalid."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

(def check-spec-interceptor
  "Interceptor that hooks clojure.spec into the application state."
  (rf/after (partial check-and-throw :cljsmix.db/db)))

(def flavor-interceptors
  "Interceptors for targeting flavors."
  [check-spec-interceptor (rf/path :flavors)])

(defn allocate-next-id
  "Returns the next ID for a given input collection."
  [col]
  ((fnil inc 0) (last (keys col))))

(rf/reg-event-db
 :initialize
 [check-spec-interceptor]
 (fn [_ _] db/default-db))

(rf/reg-event-db
 :add-flavor
 flavor-interceptors
 (fn [flavors _]
   (let [id (allocate-next-id flavors)
         new-flavors (assoc flavors id {:name "" :percent 0.0})]
     (do (println "Adding flavor:")
         (println new-flavors)
         new-flavors))))


(rf/reg-event-db
  :update-flavor
  flavor-interceptors
  (fn [flavors [_ [flavor] key new-value]]
    (assoc-in flavors [flavor key] new-value)))

(rf/reg-event-db
 :remove-flavor
 flavor-interceptors
 (fn [flavors [_ [flavor]]]
   (let [new-flavors (dissoc flavors flavor)]
     (do (println (str "Removing flavor: " flavor))
         (println flavors)
         (println new-flavors)
         new-flavors))))

(rf/reg-event-db
  :clear-flavors
  (fn [db _]
    (assoc db :flavors {})))
