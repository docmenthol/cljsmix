(ns cljsmix.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
 :flavors
 (fn [db _]
   (:flavors db)))

(rf/reg-sub
 :parameters
 (fn [db _]
   (:parameters db)))

(rf/reg-sub
 :recipes
 (fn [db _]
   (:recipes db)))
