(ns cljsmix.core
  (:require [devtools.core :as devtools]
            [reagent.core :as r]
            [cljsmix.views :as views]
            [re-frame.core :as rf]
            [cljsmix.subs]
            [cljsmix.events]))

(devtools/install!)
(enable-console-print!)

(defn start []
  (r/render-component
   [views/app]
   (. js/document (getElementById "app"))))

(defn ^:export init []
  (rf/dispatch-sync [:initialize])
  (start))

(defn stop []
  (println "stop"))
