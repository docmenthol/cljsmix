(ns cljsmix.views-desktop
  (:require [re-frame.core :as rf]
            [cljsmix.lib.semantic :as sem]
            [cljsmix.lib.semantic-helpers :as sh]))

(defn flavor-input [flavor]
  "Desktop flavor input form."
  (let [f (second flavor)]
    [:> sem/grid-row
     [:> sem/grid-column {:width 11}
      [:> sem/input {:placeholder "Unnamed Flavor"
                     :value (:name f)
                     :fluid true
                     :on-change #(rf/dispatch [:update-flavor flavor :name (.. % -target -value)])}]]
     [:> sem/grid-column {:width 3
                          :style {:padding-left 5
                                  :padding-right 20}}
      [:> sem/input {:label {:basic true
                             :content "%"}
                     :label-position "right"
                     :value (if-let [val (:percent f)]
                              (.toString val)
                              "0.0")
                     :fluid true
                     :on-change #(rf/dispatch [:update-flavor flavor :percent (js/Number (.. % -target -value))])}]]
     [:> sem/grid-column {:width 2}
      [:> sem/button {:icon "remove"
                      :on-click #(rf/dispatch [:remove-flavor flavor])}]]]))

(defn mixer []
  "Desktop parameter and flavor inputs."
  (let [parameters @(rf/subscribe  [:parameters])
        flavors    @(rf/subscribe  [:flavors])]
    [:> sem/grid {:divided "vertically"}
     [:> sem/grid-row
      [:> sem/grid-column
       [:> sem/grid {:columns 5}
        [:> sem/grid-row
         (sh/grid-cols
          sem/input
          [{:label "nic" :fluid true :value (:nic parameters)}
           {:label "tgt" :fluid true :value (:target parameters)}
           {:label "amt" :fluid true :value (:amount parameters)}
           {:label "pg"  :fluid true :value (:pg parameters)}
           {:label "vg"  :fluid true :value (:vg parameters)}])]]]]
     [:> sem/grid-row
      [:> sem/grid-column
       [:> sem/grid
        (map flavor-input flavors)]]]]))

(defn mixer-manager []
  "Desktop Add Flavor and Clear Flavors buttons."
  [:> sem/responsive {:as sem/grid-row :min-width 992}
   (sh/grid-cols
    sem/button
    [{:width 14}
     {:width 2}]
    [{:icon "add"             :on-click #(rf/dispatch [:add-flavor])}
     {:icon "trash alternate" :on-click #(rf/dispatch [:clear-flavors])}])])
